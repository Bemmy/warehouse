package inventory;

/**
 *
 * @author Be'Imnet Makonnen
 */
public class Inventory {
    
    
    private int itemCounter = 0;
    private int quantity; 
    private Item item;
    
    /**
     * An array to hold up to 100 items.
     */
    public static Item[] inventory = new Item[100];
    
    
    public Inventory(){}
    
    public Inventory(Item item){
        
        this.item = item;
    
    }
    
    public Inventory(Item[] inventory){
        this.inventory = inventory;
    }
    
    public void addItem(Item newItem) {
        inventory[itemCounter] = newItem;
        itemCounter++;
        quantity++;
    }
    
    /**
     * A method to print full inventory i.e. item's ID, name and it's quantity.
     */
   
     public void printInventory() {
        for (int i = 0; i < itemCounter; i++) {
            System.out.println("ID: " + inventory[i].getItemID()
                    + "\t Name: " + inventory[i].getName()
                    + "\t Quantity:" + inventory[i].getQuantity());
        }
    }
     
     
    /**
     * A method to get quantity of a specific item in the inventory.
     *
     * @param ID the ID of the item to be searched
     * @return the quantity of the item.
     */
     public int getItemQuantity(int ID) {
        int temp = 0;
        for (int j = 0; j < itemCounter; j++) {
            if (inventory[j].getItemID() == ID) {
                temp = inventory[j].getQuantity();
                break;
            }
        }
        return temp;
    } 
     
     
}
