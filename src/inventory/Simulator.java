/**
 *
 * @author Be'Imnet Makonnen
 */
package inventory;

import java.util.Scanner;

public class Simulator {
    
    public static void main(String[] args) {
        

        /**
         * Add a new item to the inventory. Program asks the user to enter name
         * and quantity of the item. For new item's ID, program simply
         * increments the ID of the last item added.
         */
        Scanner input = new Scanner(System.in);
        
        Inventory inventory = new Inventory();
        
        Item item1 = createItem(input);
        
        Item item2 = createItem(input);
        inventory.addItem(item2);

        /**
         * Add two more items.
         */
        Item item3 = new Item(item1.getItemID(), 5, "ET-2750 printer");
        inventory.addItem(item3);

        Item item4 = new Item(item1.getItemID(), 10, "MX-34 laptops");
        inventory.addItem(item4);

        /**
         * Print the inventory.
         */
        inventory.printInventory();

        /**
         * Find quantity of a particular item in the inventory.
         */
        System.out.println("Enter ID of the item whose quantity you want to find:");
        int temp_ID = input.nextInt();
        System.out.println("Item's quantity is: " + inventory.getItemQuantity(temp_ID));

    }
    
    public static Item createItem(Scanner input){
        
        System.out.println("Enter name of the item to add:");
        String name = input.nextLine();
        
        System.out.println("Enter the quantity:");
        int quantity = input.nextInt();
        
        System.out.println("Enter id number of the item added");
        int itemID = input.nextInt();
        
        Item item = new Item(itemID, quantity, name);//No ID, quantity or name assigned.
        
        return item;
    }

}

    

