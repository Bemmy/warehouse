/**
 *
 * @author Be'Imnet Makonnen
 */
package inventory;

public class Item {

    private int itemID;
    private String name;
    private int quantity;

    /**
     * Constructor takes in item's ID, quantity and it's name.
     *
     * @param itemID Unique ID of the item.
     * @param quantity the quantity of the item.
     * @param name the name of the item.
     */
    
    public Item(){}
    
    public Item(int itemID, int quantity, String name) {
        this.itemID = itemID;
        this.quantity = quantity;
        this.name = name;
    }
    
    public int getItemID(){
        return itemID;
    }
    
    public void setItemID(int itemID){
        this.itemID = itemID;
    }
    
    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public int getQuantity(){
        return quantity;
    }
    
    public void setQuantity(int quantity){
        this.quantity = quantity;
    }
   
}
